# Instructions

Use Javascript to resize the button when the mouse hovers over it.

## Bonus

Add a second button and show an alternative way of resizing the button on hover
