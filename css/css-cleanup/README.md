# Instructions

Review the CSS included and optimise where possible.

## Bonus

*   Think you've got a keen eye for design? Make it look better while you're at it.
*   Create a new file and repeat the process in SCSS
