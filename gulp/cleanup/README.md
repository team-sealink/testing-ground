# Instructions

Review the included `gulpfile.js`, cleanup the formatting and add comments to show you understand what each step is doing

## Bonus

Duplicate `gulpfile.js` and apply further optimisations. Feel free to add  / remove / replace as necessary.
