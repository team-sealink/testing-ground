var _browserSupport;
var _paths;
var autoprefixer;
var clean;
var concat;
var fontello;
var gulp;
var gutil;
var modernizr;
var path;
var rename;
var runSequence;
var sass;
var watch;

gulp = require('gulp');
concat = require('gulp-concat');
watch = require('gulp-watch');
sass = require('gulp-sass');
autoprefixer = require('gulp-autoprefixer');
runSequence = require('run-sequence');
gutil = require('gulp-util');
path = require('path');
modernizr = require('gulp-modernizr');
fontello = require('gulp-fontello');
rename = require('gulp-rename');
clean = require('gulp-clean');

_browserSupport = ['> 5%', 'last 4 versions'];

_paths = {
  clean_dirs: [path.resolve('./public/dist'), path.resolve('./assets/src/scss/glyphs')],
  all_src: path.resolve('./assets/src/**/*.*'),
  styles: [path.resolve('./assets/src/scss/**/*.scss'), path.resolve('./assets/src/scss/**/*.sass')],
  scripts: path.resolve('./assets/src/js/**/*.*'),
  build_css: path.resolve('./public/dist/css'),
  build_js: path.resolve('./public/dist/js'),
  built: path.resolve('./public/dist'),
  outputGlyphCss: path.resolve('./assets/src/scss/glyphs'),
  outputGlyphfonts: path.resolve('./public/dist/fonts'),
  glyphsConfig: path.resolve('./assets/src/fonts/config.json'),
  sassIncludes: ['node_modules', 'node_modules/support-for/sass', 'node_modules/bootstrap-sass/assets/stylesheets'],
  js_libs: 'node_modules/jquery/dist/jquery.min.js',
  jsDistFile: 'app.js'
};

gulp.task('modernizr', function() {
  return gulp.src(_paths.all_src).pipe(modernizr('modernizr-custom.js', {
    'tests': ['flexbox', 'flexboxlegacy'],
    'options': ['html5shiv', 'setClasses']
  })).pipe(gulp.dest(_paths.build_js));
});

gulp.task('scripts', function() {
  return gulp.src(_paths.scripts).pipe(concat(_paths.jsDistFile)).pipe(gulp.dest(_paths.build_js));
});

gulp.task('glyphs', function() {
  return gulp.src(_paths.glyphsConfig, {
    base: process.cwd()
  }).pipe(fontello({
    assetsOnly: true,
    font: 'fonts'
  })).pipe(rename(function(path) {
    var cssFile;
    cssFile = path.extname === '.css';
    path.dirname = cssFile ? _paths.outputGlyphCss : _paths.outputGlyphfonts;
    if (cssFile) {
      path.extname = '.scss';
      return path.basename = '_' + path.basename;
    }
  })).pipe(gulp.dest('/'));
});

gulp.task('js_libs', function() {
  return gulp.src(_paths.js_libs).pipe(rename({
    dirname: '.'
  })).pipe(gulp.dest(_paths.build_js));
});

gulp.task('styles', function() {
  return gulp.src(_paths.styles).pipe(sass({
    includePaths: _paths.sassIncludes,
    outputStyle: 'expanded'
  }).on('error', sass.logError)).pipe(autoprefixer({
    browsers: _browserSupport
  })).pipe(gulp.dest(_paths.build_css));
});

gulp.task('watch', function() {
  watch(_paths.styles, {
    usePolling: true
  }, function() {
    return gulp.start('styles');
  });
  return watch(_paths.scripts, {
    usePolling: true
  }, function() {
    return gulp.start('scripts');
  });
});

gulp.task('clean', function() {
  return gulp.src(_paths.clean_dirs, {
    read: false
  }).pipe(clean());
});

gulp.task('build', function(cb) {
  return runSequence(['clean'], ['js_libs', 'modernizr', 'glyphs'], ['styles', 'scripts'], cb);
});

gulp.task('default', function(cb) {
  return runSequence(['build'], ['watch']);
});
