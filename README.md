
# Welcome to the SeaLink Proving Ground

This is a space to test your skills and show us what you're made of.

The tests here are designed to provide you with real-world scenarios for you to showcase your abilities.

The minimum requirements for these tests are **Production-ready Code** and the only real requirement on the other end of the scale is time. Do what you feel is necessary to change, adapt, delete, optimise and refine.

**Some tests are quick and you will have them done within 5 minutes. Others will take longer however you're expected to spend no more than 4 hours on any given test. This will give you sufficient time to present a good quality, working solution.**

## Requirements

*   Please fork this repository and create a new branch with your name and date in the following format: `john-doe_20161103`
*   Add a new file `SUMMARY.MD` to each test you complete briefly describing what you have done
*   You can use any languages, frameworks or libraries you think will achieve the objectives
*   Follow Agile coding principles, do not over-engineer the task
*   We must be able to build and/or run your code without any errors (be it at compilation time or runtime).
*   Please submit your results by providing us with the link to your forked repo.
*   **You must check in your code at least once every 30mins.**



## Tests

Note: This list is dynamic and will change over time.

-   General Knowledge Questions
-   CSS
    -   Refactoring
    -   Positioning (Floats, Flexbox, Grid)
    -   Animations
    -   Frameworks (Bootstrap, Foundation, ... )
-   HTML
    -   Cleanup
-   JS
    -   Interactions
    -   Animations
-   JSON & XML
-   Build Tools
    -   Task Runners (Gulp, Grunt, ... )
    -   Package Managers (Bower, NPM, ... )
    -   Module loaders (Browserify, Webpack, ... )
-   Tests
    -   
-   GIT
    -   Create new repository
    -   Create Feature branch
    -   Add README
    -   Commit changes
    -   Add Changelog
    -   Submit Pull Request
-   SEO
-   UI
-   UX
