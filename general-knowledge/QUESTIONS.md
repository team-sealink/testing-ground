#Front-end Job Interview Questions

This file contains a number of front-end interview questions that can be used when vetting potential candidates. It is by no means recommended to use every single question here on the same candidate (that would take hours). Choosing a few items from this list should help you vet the intended skills you require.

**Note:** Keep in mind that many of these questions are open-ended and could lead to interesting discussions that tell you more about the person's capabilities than a straight answer would.

## Table of Contents

1.  [General Questions](#general-questions)
2.  [HTML Questions](#html-questions)
3.  [CSS Questions](#css-questions)
4.  [JS Questions](#js-questions)
5.  [Testing Questions](#testing-questions)
6.  [Performance Questions](#performance-questions)
7.  [Network Questions](#network-questions)
8.  [Coding Questions](#coding-questions)
9.  [Fun Questions](#fun-questions)


### General Questions:

*   Q. Describe something new you've learnt this week?
    A.

*   Q. What excites or interests you about the web?
    A.

*   Q. Describe a recent technical challenge you experienced and how you resolved it?
    A.

*   Q. Tell us about your preferred development environment.
    A.

*   Q. Which version control systems are you familiar with?
    A.

*   Q. Can you describe your workflow when you create a web page?
    A.

*   Q. If you have 5 different stylesheets, how would you best integrate them into one page?
    A.

*   Q. Can you describe the difference between progressive enhancement and graceful degradation?
    A.

*   Q. How would you optimize a website's assets/resources?
    A.

*   Q. How many resources will a browser download from a given domain at a time?
    A.

*   Q. What UI, Security, Performance, SEO, Maintainability or Technology considerations do you make while building a web application or site?
    A.

*   Q. Name 3 ways to decrease page load (perceived or actual load time).
    A.

*   Q. If you jumped on a project and they used tabs and you used spaces, what would you do?
    A.

*   Q. If you could master one technology this year, what would it be?
    A.

*   Q. Explain the importance of standards and standards bodies.
    A.

*   Q. What is Flash of Unstyled Content? How do you avoid FOUC?
    A.

*   Q. Explain what ARIA and screenreaders are, and how to make a website accessible.
    A.

*   Q. Explain some of the pros and cons for CSS animations versus JavaScript animations.
    A.

*   Q. What does CORS stand for and what issue does it address?
    A.


### HTML Questions:

*   Q. What does a `doctype` do?
    A.

*   Q. What's the difference between full standards mode, almost standards mode and quirks mode?
    A.

*   Q. What's the difference between HTML and XHTML?
    A.

*   Q. How do you serve a page with content in multiple languages?
    A.

*   Q. What kind of things must you be wary of when design or developing for multilingual sites?
    A.

*   Q. What are `data-` attributes good for?
    A.

*   Q. Consider HTML5 as an open web platform. What are the building blocks of HTML5?
    A.

*   Q. Describe the difference between a `cookie`, `sessionStorage` and `localStorage`.
    A.

*   Q. Describe the difference between `<script>`, `<script async>` and `<script defer>`.
    A.

*   Q. Why is it generally a good idea to position CSS `<link>`s between `<head></head>` and JS `<script>`s just before `</body>`? Do you know any exceptions?
    A.

*   Q. What is progressive rendering?
    A.

*   Q. Have you used different HTML templating languages before?
    A.


#### CSS Questions:

*   Q. What is the difference between classes and IDs in CSS?
    A.

*   Q. What's the difference between "resetting" and "normalizing" CSS? Which would you choose, and why?
    A.

*   Q. Describe Floats and how they work.
    A.

*   Q. Describe z-index and how stacking context is formed.
    A.

*   Q. Describe BFC(Block Formatting Context) and how it works.
    A.

*   Q. What are the various clearing techniques and which is appropriate for what context?
    A.

*   Q. Explain CSS sprites, and how you would implement them on a page or site.
    A.

*   Q. What are your favourite image replacement techniques and which do you use when?
    A.

*   Q. How would you approach fixing browser-specific styling issues?
    A.

*   Q. How do you serve your pages for feature-constrained browsers? What techniques/processes do you use?
      A.

*   Q. What are the different ways to visually hide content (and make it available only for screen readers)?
    A.

*   Q. Have you ever used a grid system, and if so, what do you prefer?
    A.

*   Q. Have you used or implemented media queries or mobile specific layouts/CSS?
    A.

*   Q. Are you familiar with styling SVG?
    A.

*   Q. How do you optimize your webpages for print?
    A.

*   Q. What are some of the "gotchas" for writing efficient CSS?
    A.

*   Q. What are the advantages/disadvantages of using CSS preprocessors?
    A.

*   Q. Describe what you like and dislike about the CSS preprocessors you have used.
    A.

*   Q. How would you implement a web design comp that uses non-standard fonts?
    A.

*   Q. Explain how a browser determines what elements match a CSS selector.
    A.

*   Q. Describe pseudo-elements and discuss what they are used for.
    A.

*   Q. Explain your understanding of the box model and how you would tell the browser in CSS to render your layout in different box models.
    A.

*   Q. What does ```* { box-sizing: border-box; }``` do? What are its advantages?
    A.

*   Q. List as many values for the display property that you can remember.
    A.

*   Q. What's the difference between inline and inline-block?
    A.

*   Q. What's the difference between a relative, fixed, absolute and statically positioned element?
    A.

*   Q. The 'C' in CSS stands for Cascading.  How is priority determined in assigning styles (a few examples)?  How can you use this system to your advantage?
    A.

*   Q. What existing CSS frameworks have you used locally, or in production? How would you change/improve them?
    A.

*   Q. Have you Used CSS Flexbox or Grid specs?
    A.

*   Q. How is responsive design different from adaptive design?
    A.

*   Q. Have you ever worked with retina graphics? If so, when and what techniques did you use?
    A.

*   Q. Is there any reason you'd want to use `translate()` instead of *absolute positioning*, or vice-versa? And why?



#### JS Questions:

*   Q. Explain event delegation
    A.

*   Q. Explain how `this` works in JavaScript
    A.

*   Q. Explain how prototypal inheritance works
    A.

*   Q. What do you think of AMD vs CommonJS?
    A.

*   Q. Explain why the following doesn't work as an IIFE: `function foo(){ }();`.
    A.

*   Q. What needs to be changed to properly make it an IIFE?
    A.

*   Q. What's the difference between a variable that is: `null`, `undefined` or undeclared?
    A.

*   Q. * How would you go about checking for a variable that is: `null`, `undefined` or undeclared?
    A.

*   Q. What is a closure, and how/why would you use one?
    A.

*   Q. What's a typical use case for anonymous functions?
    A.

*   Q. How do you organize your code? (module pattern, classical inheritance?)
    A.

*   Q. What's the difference between host objects and native objects?
    A.

*   Q. Difference between: `function Person(){}`, `var person = Person()`, and `var person = new Person()`?
    A.

*   Q. What's the difference between `.call` and `.apply`?
    A.

*   Q. Explain `Function.prototype.bind`.
    A.

*   Q. When would you use `document.write()`?
    A.

*   Q. What's the difference between feature detection, feature inference, and using the UA string?
    A.

*   Q. Explain Ajax in as much detail as possible.
    A.

*   Q. What are the advantages and disadvantages of using Ajax?
    A.

*   Q. Explain how JSONP works (and how it's not really Ajax).
    A.

*   Q. Have you ever used JavaScript templating? If so, what libraries have you used?
    A.

*   Q. Explain "hoisting".
    A.

*   Q. Describe event bubbling.
    A.

*   Q. What's the difference between an "attribute" and a "property"?
    A.

*   Q. Why is extending built-in JavaScript objects not a good idea?
    A.

*   Q. Difference between document load event and document DOMContentLoaded event?
    A.

*   Q. What is the difference between `==` and `===`?
    A.

*   Q. Explain the same-origin policy with regards to JavaScript.
    A.

*   Q. Make this work:
```javascript
duplicate([1,2,3,4,5]); // [1,2,3,4,5,1,2,3,4,5]
```
  A.

*   Q. Why is it called a Ternary expression, what does the word "Ternary" indicate?
    A.

*   Q. What is `"use strict";`? what are the advantages and disadvantages to using it?
    A.

*   Q. Create a for loop that iterates up to `100` while outputting **"fizz"** at multiples of `3`, **"buzz"** at multiples of `5` and **"fizzbuzz"** at multiples of `3` and `5`
    A.

*   Q. Why is it, in general, a good idea to leave the global scope of a website as-is and never touch it?
    A.

*   Q. Why would you use something like the `load` event? Does this event have disadvantages? Do you know any alternatives, and why would you use those?
    A.

*   Q. Explain what a single page app is and how to make one SEO-friendly.
    A.

*   Q. What is the extent of your experience with Promises and/or their polyfills?
    A.

*   Q. What are the pros and cons of using Promises instead of callbacks?
    A.

*   Q. What are some of the advantages/disadvantages of writing JavaScript code in a language that compiles to JavaScript?
    A.

*   Q. What tools and techniques do you use debugging JavaScript code?
    A.

*   Q. What language constructions do you use for iterating over object properties and array items?
    A.

*   Q. Explain the difference between mutable and immutable objects.
    A.

*   Q. What is an example of an immutable object in JavaScript?
    A.

*   Q. What are the pros and cons of immutability?
    A.

*   Q. How can you achieve immutability in your own code?
    A.

*   Q. Explain the difference between synchronous and asynchronous functions.
    A.

*   Q. What is event loop? What is the difference between call stack and task queue?
    A.

*   Q. Explain the differences on the usage of `foo` between `function foo() {}` and `var foo = function() {}`



### Testing Questions:

*   Q. What are some advantages/disadvantages to testing your code?
    A.

*   Q. What tools would you use to test your code's functionality?
    A.

*   Q. What is the difference between a unit test and a functional/integration test?
    A.

*   Q. What is the purpose of a code style linting tool?
    A.



### Performance Questions:

*   Q. What tools would you use to find a performance bug in your code?
    A.

*   Q. What are some ways you may improve your website's scrolling performance?
    A.

*   Q. Explain the difference between layout, painting and compositing.



### Network Questions:

*   Q. Traditionally, why has it been better to serve site assets from multiple domains?
    A.

*   Q. Do your best to describe the process from the time you type in a website's URL to it finishing loading on your screen.
    A.

*   Q. What are HTTP methods? List all HTTP methods that you know, and explain them.
    A.







### Coding Questions:

*Question: What is the value of `foo`?*
```javascript
var foo = 10 + '20';
```

*Question: How would you make this work?*
```javascript
add(2, 5); // 7
add(2)(5); // 7
```

*Question: What value is returned from the following statement?*
```javascript
"i'm a lasagna hog".split("").reverse().join("");
```

*Question: What is the value of `window.foo`?*
```javascript
( window.foo || ( window.foo = "bar" ) );
```

*Question: What is the outcome of the two alerts below?*
```javascript
var foo = "Hello";
(function() {
  var bar = " World";
  alert(foo + bar);
})();
alert(foo + bar);
```

*Question: What is the value of `foo.length`?*
```javascript
var foo = [];
foo.push(1);
foo.push(2);
```

*Question: What is the value of `foo.x`?*
```javascript
var foo = {n: 1};
var bar = foo;
foo.x = foo = {n: 2};
```

*Question: What does the following code print?*
```javascript
console.log('one');
setTimeout(function() {
  console.log('two');
}, 0);
console.log('three');
```

### Fun Questions:

*   Q. What's a cool project that you've recently worked on?
    A.

*   Q. What are some things you like about the developer tools you use?
    A.

*   Q. Who inspires you in the front-end community?
    A.

*   Q. Do you have any pet projects? What kind?
    A.

*   Q. What's your favorite feature of Internet Explorer?
    A.

*   Q. How do you like your coffee?
    A.
