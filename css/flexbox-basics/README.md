# Instructions

Complete the following tasks to demonstrate a basic understanding of Flexbox
*   Use flexbox to evenly distribute the items in the container
*   Change the background colour of the middle item to red
*   Reorder the items so the text matches their position
*   Move the Right item to a new row

## Bonus
