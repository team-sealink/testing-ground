# Instructions

Complete the following tasks using only CSS

*   Position the green box in the lower right corner
*   Add rounded corners
*   Reposition the green box to the middle of the blue box

## Bonus

Duplicate the elements and show an alternative way of achieving the same effects
